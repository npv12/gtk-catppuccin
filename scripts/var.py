import os


repo_dir = os.getcwd()
work_dir = f"{repo_dir}/colloid"
src_dir = f"{work_dir}/src"
tmp_dir = f"{work_dir}/tmp"
theme_name = "Catppuccin"

color_map = {
    'lavender': 'default',
    'mauve': 'purple',
    'pink': 'pink',
    'red': 'red',
    'peach': 'orange',
    'yellow': 'yellow',
    'green': 'green',
    'teal': 'teal'}
